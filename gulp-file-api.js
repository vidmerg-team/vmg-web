/** 
 * Copy FileAPI files
 * @module
 */
'use strict';

exports.run = function(gulp, pth) {
  gulp.task('copy_file_api', [], function() {
    return gulp.src(pth.fileApiRoot + '*.*')
      .pipe(gulp.dest(pth.dst + 'libs/file-api/'));
  });
};

module.exports = exports;
