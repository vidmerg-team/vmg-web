module.exports = {
  loader: 'enh-loader',
  player: 'enh-player',
  cellSide1: 'page-enhance__cell-side-1',
  cellSide2: 'page-enhance__cell-side-2'
};
