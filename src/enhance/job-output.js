/**
 * Job output
 * @module
 */

/**
 * Job output ('completed')
 * @constructor
 */
var Mdl = function(data) {
  Object.keys(data).forEach(this.buildProp.bind(this, data));


};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey) {
  this[propKey] = data[propKey];
};

/**
 * Get first media file, usually only one per output
 */
Mdl.prototype.getFirstMediaFile = function() {
  return this.media_spec_item.file_output_arr[0].media_file_item;
};

/**
 * Create an instance
 */
exports.init = function() {
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
