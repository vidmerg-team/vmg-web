/** 
 * @todo: #24! Get duration of video to cut; start and stop; with or without loading full video
 * @todo: #34! Load episode info
 *             movie_template.name, .duration_of_episodes,
 *             episode_template.name, story, conds, order_in_movie
 *             media_spec
 *             job_output.id_of_job_status, status_detail
 *             file_output_arr
 *             media_file.url, .size, .duration
 * @todo: #33! Show media url (when a job is ready);
 * @todo: #33! Show cutline with start, stop points
 *             After submit, send a request with start, stop points to crete job_cut
 *             Check this job every N seconds, while status = 'Complete' (set media_spec.is_ready = true)
 *             Re-load the video (with this url): cutted video already
 * @module
 */

var jobOutputChecker = require('./job-output-checker');
var dhr = require('../vmg-helpers/dom');
var ahr = require('../vmg-helpers/app');
var srv = require('../vmg-services/srv');
var pblWorkspace = require('../common/workspace');
var playCut = require('play-cut/index');
var mdlEpisodeBid = require('./episode-bid');
var hprJobOutput = require('./job-output');
var cls = require('./cls');
var markups = {};

var Mdl = function(zpath) {
  console.log('wsp', arguments);
  pblWorkspace.apply(this, [cls, markups, zpath]);
  this.esc = {
    notif: dhr.getElem('.' + this.cls.notif),
    loader: dhr.getElem('.' + this.cls.loader),
    player: dhr.getElem('.' + this.cls.player),
    cellSide1: dhr.getElem('.' + this.cls.cellSide1),
    cellSide2: dhr.getElem('.' + this.cls.cellSide2)
  };

  this.idOfMediaSpec = null;
  // set after CheckJobOutput
  this.jobOutput = null;

  // handlePostJobCut
  this.jobCut = null;

  /**
   * Episode bid
   */
  this.episodeBid = null;

  /**
   * Result of cropping, seconds (double)
   * @type {Number}
   */
  this.crop_result_start = null;

  /**
   * Result of cropping
   * @type {Number}
   */
  this.crop_result_stop = null;

  /**
   * Button to crop
   */
  this.btnCrop = document.createElement('button');

  $(this.btnCrop)
    .css({
      padding: '6px'
    })
    .hide()
    .html('Crop')
    .on('click', this.cutVideo.bind(this));

  this.esc.cellSide2.appendChild(this.btnCrop);
};

ahr.inherits(Mdl, pblWorkspace);

Mdl.prototype.handleGetJobCut = function(err, jobCut) {
  if (err) {
    this.showNotif(err);
    return;
  }

  console.log(jobCut);

  if (jobCut.id_of_job_status === 'Error') {
    // TODO: #43! recreate the job
    this.showNotif(new Error('A job is failed: ' + jobCut.status_detail + '<br>Please contact with administration'));
    return;
  }
  //https://github.com/videojs/video.js/blob/stable/docs/api/vjs.Player.md
  if (jobCut.id_of_job_status === 'Complete') {
    window.location.href = './cabinet.html';

    //    var needMediaFileCut = jobCut.media_spec_item.file_cut_arr[0].media_file_item;
    // TODO: #33! Show button to attach video to episode   
    // And remove cutted version, to cut again from source (in future);
    // Get url,
    // Change video source url
    // Hide slider
    return;
  }

  // check one more
  window.setTimeout(this.checkJobCut.bind(this), 1500);
};

Mdl.prototype.checkJobCut = function() {
  srv.r1007(this.idOfMediaSpec, this.handleGetJobCut.bind(this));
};

Mdl.prototype.handlePostJobCut = function(err, jobCut) {
  if (err) {
    return this.showNotif(err);
  }

  this.jobCut = jobCut;

  // after posting - retry with progress bar
  console.log('jobCut', jobCut);
  // TODO: #33! Change loader
  window.setTimeout(this.checkJobCut.bind(this), 1500);
};

// allow this event only full video download
// TODO: #43! Or append this event dinamically after vide downloading
// TODO: #43! Add values for start and stop points dynamically
Mdl.prototype.cutVideo = function() {
  // hide prev errors
  dhr.hideElems(this.esc.notif);
  var cuttingStart = this.crop_result_start;
  var cuttingStop = this.crop_result_stop;

  var errValid = [];

  if (typeof cuttingStart !== 'number') {
    errValid.push('Please select a region to crop');
  }
  if (typeof cuttingStop !== 'number') {
    errValid.push('Please select a region to crop');
  }

  // convert seconds to ms
  var episodeDuration = this.episodeBid.getEpisodeDuration();

  var episodeVariation = 1000; // 1 second

  var cuttingDiff = cuttingStop - cuttingStart;

  if (cuttingDiff < (episodeDuration - episodeVariation) || cuttingDiff > (episodeDuration + episodeVariation)) {
    errValid.push('Allowed duration (difference between values): from ' + (episodeDuration - episodeVariation) + ' to ' + (episodeDuration + episodeVariation));
  }

  if (errValid.length > 0) {
    return this.showNotif(new Error(errValid.join('<br>')));
  }


  // hide player, show loader
  dhr.hideElems(this.esc.player);
  $(this.btnCrop).hide();

  dhr.html(this.esc.loader, 'processing...');
  dhr.showElems(this.esc.loader);

  var jobCut = {
    id_of_media_spec: this.jobOutput.id_of_media_spec,
    cutting_start: cuttingStart,
    cutting_stop: cuttingStop
  };
  //    var elemCuttingStart = dhr.getElem('.' + clsNotif);
  //    var elemNotif = dhr.getElem('.' + clsNotif);
  // send it to the server
  srv.w2005(jobCut, this.handlePostJobCut.bind(this));
};

// TODO: #33! change to real data
//var loadFrames = function() {
//  return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14].map(function(item) {
//    return {
//      url: 'https://s3-eu-west-1.amazonaws.com/vmg-dev-frame/165709257/000' + ('0' + item).slice(-2) + '.png'
//    };
//    //    return 'https://s3-eu-west-1.amazonaws.com/vmg-dev-frame/792368719/0000' + item + '.png';
//  });
//};

/**
 * Handle result of crop
 * @param {Number} start - start pos, in seconds (double)
 * @param {Number} stop - stop pos, in seconds (double)
 */
Mdl.prototype.handleRangeResult = function(start, stop) {
  this.crop_result_start = start * 1000;
  this.crop_result_stop = stop * 1000;
};

/**
 * Filter: leave only N elements of array (plus zero elem)
 * @type {Array} arr - Initial array
 * @type {Number} n - Count of elements to return;
 * @returns {Array}
 */
function leaveOnlyNElems(arr, n) {
  // leave zero and every p-element in array
  var p = Math.round(arr.length / n);
  console.log('ind count: ', arr.length);
  return arr.filter(function(elem, ind) {
    if (ind % p === 0) {
      console.log('ind filter: ', ind);
      return true;
    }
    return false;
  });
}

Mdl.prototype.showPlayer = function() {
  var mediaFile = this.jobOutput.getFirstMediaFile();

  // play-cut <<<
  var sources = [{
    url: mediaFile.url,
    mime_type: mediaFile.id_of_container_format
  }];

  var frames = this.jobOutput.media_spec_item.media_frame_arr;

  // filter some frames: for the play-cut module: 8-14 frames ~10
  // count of frames ~45
  //var p = ahr.toInt(frames.length / 10);
  var pluginFrames;
  if (frames.length > 14) {
    pluginFrames = leaveOnlyNElems(frames, 10);
  } else {
    pluginFrames = frames;
  }

  var playCutBlock = playCut.init(sources,
    pluginFrames,
    this.episodeBid.getEpisodeDuration() / 1000,
    this.handleRangeResult.bind(this));
  // play-cut >>>

  $(playCutBlock).css({
    position: 'relative'
  });

  dhr.showElems(this.esc.player);
  dhr.html(this.esc.player, playCutBlock);

  $(this.btnCrop).show();
};

/**
 * Handle converted media file
 *     from job_output
 */
Mdl.prototype.handleJobOutput = function(err, jobOutputData) {
  if (err) {
    // TODO: #33! show notif about error - job recreate here??
    dhr.hideElems(this.esc.loader);
    dhr.hideElems(this.esc.player);
    this.showNotif(err);
    return;
  }

  this.jobOutput = hprJobOutput.init(jobOutputData);

  dhr.hideElems(this.esc.loader);
  this.showPlayer();
  //  console.log('mediaFile', mediaFile);
};

Mdl.prototype.handleBidInfo = function(err, episodeBidData) {
  if (err) {
    this.showNotif(err);
    return;
  }

  this.episodeBid = mdlEpisodeBid.init(episodeBidData);
  //console.log('episode bid', episodeBid);

  jobOutputChecker.run(this.idOfMediaSpec, this.handleJobOutput.bind(this));
};

Mdl.prototype.getBidInfo = function() {
  srv.r1008(this.idOfMediaSpec, this.handleBidInfo.bind(this));
};

Mdl.prototype.showNotif = function(err) {
  dhr.html(this.esc.notif, err.message);
  dhr.showElems(this.esc.notif);
};

Mdl.prototype.loadIdOfMediaSpec = function(next) {
  var mParam = ahr.getQueryParam('m');
  mParam = ahr.toInt(mParam);

  if (!mParam) {
    alert('No param in url: ?m=123 as integer');
    return;
  }

  this.idOfMediaSpec = mParam;
  next();
};

Mdl.prototype.authFlowSelector = function() {
  if (this.userSession) {
    this.userSession.showAuth(this.last);
    this.getBidInfo();
  } else {
    this.waitUserLogin();
    // show message and apply events and login buttons with authFlow
  }
};

Mdl.prototype.startFlow = function() {
  var appFlow =
    this.loadIdOfMediaSpec.bind(this,
      this.waitDocReady.bind(this,
        this.addEvents.bind(this,
          this.loadSid.bind(this,
            // two flows - auth=yes and auth=no
            this.handleSid.bind(this,
              this.authFlowSelector.bind(this)
            )))));

  appFlow();
};

exports.init = function() {
  // add methods
  var obj = Object.create(Mdl.prototype);
  // add props
  Mdl.apply(obj, arguments);
  // return created object
  return obj;
  //  return new Mdl.bind(this, arguments);
};

module.exports = exports;
