/**
 * Episode bid
 * @module
 */

var ahr = require('../vmg-helpers/app');

/**
 * Episode bid
 * @constructor
 */
var Mdl = function(data){
  Object.keys(data).forEach(this.buildProp.bind(this, data));
  
  
};

/**
 * Build a prop for a model
 */
Mdl.prototype.buildProp = function(data, propKey){
  this[propKey] = data[propKey];
};

/**
 * Calc episode duration in milliseconds
 * @returns {Number}
 */
Mdl.prototype.getEpisodeDuration = function() {
  return ahr.toInt(this.episode_template_item.movie_template_item.duration_of_episodes) * 1000;
};

/**
 * Create an instance
 */
exports.init = function(){
  var obj = Object.create(Mdl.prototype);
  Mdl.apply(obj, arguments);
  return obj;
};

module.exports = exports;
