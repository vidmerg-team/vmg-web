var path = require('path');

var cropDir = path.join('node_modules',
  'play-cut',
  'node_modules',
  'movie-crop-slider');

exports.run = function(gulp, pth) {
  gulp.task('copy_crop_img', function() {
    return gulp.src(path.join(cropDir, 'img', '*.*'))
      .pipe(gulp.dest(path.join(pth.dst, 'libs', 'movie-crop-slider', 'img')));
  });

  gulp.task('copy_crop_css', function() {
    return gulp.src(path.join(cropDir, 'css', '*.css'))
      .pipe(gulp.dest(path.join(pth.dst, 'libs', 'movie-crop-slider', 'css')));
  });

  gulp.task('copy_crop', ['copy_crop_img', 'copy_crop_css']);
};

module.exports = exports;
